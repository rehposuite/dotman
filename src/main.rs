use configparser::ini::Ini;
use dirs::config_dir;
use dirs::home_dir;
use nix::unistd::Uid;
use pathdiff::diff_paths;
use rm_rf::remove as rm;
use std::env::args;
use std::fs::canonicalize;
use std::fs::create_dir_all;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::process::exit;
use std::process::Command;
use walkdir::WalkDir;

fn main() {
    if Uid::effective().is_root() {
        eprintln!("The program cannot be run as root");
        exit(2);
    }

    let configdirectory = match config_dir() {
        Some(x) => x.into_os_string().into_string().unwrap(),
        None => "Config directory could not be found".to_string(),
    };

    let mut config = Ini::new();
    let _loadedconfig = config.load(format!("{configdirectory}/dotman.ini"));

    let homedirectory = match config.get("dirs", "homedirectory") {
        Some(x) => x,
        None => match home_dir() {
            Some(x) => x.into_os_string().into_string().unwrap(),
            None => "Home directory could not be found".to_string(),
        },
    };

    let dotfilespath = match config.get("dirs", "dotfilespath") {
        Some(x) => x,
        None => format!("{homedirectory}/dotfiles"),
    };
    let homepath = match config.get("dirs", "homepath") {
        Some(x) => x,
        None => format!("{dotfilespath}/home/"),
    };
    let slashpath = match config.get("dirs", "slashpath") {
        Some(x) => x,
        None => format!("{dotfilespath}/slash/"),
    };

    let dotmanurl = "https://gitlab.com/rehposuite/dotman/-/raw/main/target/release/dotman";

    let mut args: Vec<String> = args().collect();
    if args.len() > 1 {
        let action = &args[1].clone();
        args.remove(0);
        args.remove(0);
        run(
            &action,
            &args,
            &homedirectory,
            &homepath,
            &slashpath,
            &dotmanurl,
        );
    } else {
        eprintln!("No argument specified");
        exit(2);
    }
}

fn run(
    action: &str,
    other: &Vec<String>,
    homedirectory: &str,
    homepath: &str,
    slashpath: &str,
    dotmanurl: &str,
) {
    match action {
        "-u" | "update" => update(&dotmanurl),
        "-h" | "--help" | "help" => help(),

        "-p" | "pull" => pull(&homepath, &homedirectory, false),
        "-rp" | "rootpull" => pull(&homepath, "/root", true),
        "-sp" | "slashpull" => pull(&slashpath, "/", true),

        "-pu" | "push" => push(&homedirectory, &homepath, false),
        "-spu" | "slashpush" => push("/", &slashpath, true),

        "-a" | "add" => add(&homedirectory, &homepath, &other),
        "-sa" | "slashadd" => add("/", &slashpath, &other),

        "-r" | "remove" => remove(&homedirectory, &homepath, &other),
        "-sr" | "slashremove" => remove("/", &slashpath, &other),

        _ => {
            eprintln!("Invalid argument");
            exit(2);
        }
    }
}

fn pull(from: &str, to: &str, sudo: bool) {
    let mut file = File::create("/tmp/dotman").unwrap();
    for path in WalkDir::new(from) {
        match path {
            Ok(x) => {
                if !x.path().is_dir() {
                    let relativepath = diff_paths(&x.path().display().to_string(), &from)
                        .unwrap()
                        .display()
                        .to_string();

                    if relativepath != "" {
                        let _newfile = file.write_all(format!("{relativepath}\n").as_bytes());
                    }
                }
            }
            Err(x) => {
                eprintln!("{x} not found");
                exit(2);
            }
        }
    }

    if sudo == false {
        let _rsync = Command::new("rsync")
            .arg("-trvlp")
            .arg("--files-from")
            .arg("/tmp/dotman")
            .arg(&from)
            .arg(&to)
            .status()
            .unwrap()
            .success();
    } else {
        let _rsync = Command::new("sudo")
            .arg("rsync")
            .arg("-trvlp")
            .arg("--files-from")
            .arg("/tmp/dotman")
            .arg(&from)
            .arg(&to)
            .status()
            .unwrap()
            .success();
    }
}

fn push(from: &str, to: &str, sudo: bool) {
    let mut file = File::create("/tmp/dotman").unwrap();
    for path in WalkDir::new(to) {
        match path {
            Ok(x) => {
                if !x.path().is_dir() {
                    let relativepath = diff_paths(&x.path().display().to_string(), &to)
                        .unwrap()
                        .display()
                        .to_string();

                    if relativepath != "" {
                        let _newfile = file.write_all(format!("{relativepath}\n").as_bytes());
                    }
                }
            }
            Err(x) => {
                eprintln!("{x} not found");
                exit(2);
            }
        }
    }

    if sudo == false {
        let _rsync = Command::new("rsync")
            .arg("-trvlp")
            .arg("--files-from")
            .arg("/tmp/dotman")
            .arg(&from)
            .arg(&to)
            .status()
            .unwrap()
            .success();
    } else {
        let _rsync = Command::new("sudo")
            .arg("rsync")
            .arg("-trvlp")
            .arg("--files-from")
            .arg("/tmp/dotman")
            .arg(&from)
            .arg(&to)
            .status()
            .unwrap()
            .success();
    }
}

fn add(from: &str, to: &str, other: &Vec<String>) {
    if other.len() == 0 {
        eprintln!("No file or directory specified");
        exit(2);
    }
    for path in other {
        match canonicalize(&path) {
            Ok(x) => {
                let relativepath = diff_paths(&x, &from).unwrap();
                let newpath = format!("{to}{}", relativepath.display());
                if Path::new(&path).is_file() == true {
                    let parent = relativepath.parent();
                    match create_dir_all(format!("{to}{}", parent.unwrap().display())) {
                        Ok(_) => {
                            let _file = File::create(&newpath);
                            println!("Created file {newpath}");
                        }
                        Err(_) => {
                            eprintln!("Could not create file {newpath}");
                            exit(2);
                        }
                    }
                } else {
                    match create_dir_all(&newpath) {
                        Ok(_) => println!("Created directory {newpath}"),
                        Err(_) => {
                            eprintln!("Could not create directory {newpath}");
                            exit(2);
                        }
                    }
                }
            }
            Err(_) => {
                eprintln!("File or directory does not exist");
                exit(2);
            }
        }
    }
}

fn remove(from: &str, to: &str, other: &Vec<String>) {
    if other.len() == 0 {
        eprintln!("No file or directory specified");
        exit(2);
    }
    for path in other {
        match canonicalize(&path) {
            Ok(x) => {
                let relativepath = diff_paths(&x, &from).unwrap();
                let newpath = format!("{to}{}", relativepath.display());
                if Path::new(&newpath).exists() {
                    let _removed = rm(&newpath);
                    println!("Removed {newpath}");
                } else {
                    eprintln!("Cannot remove {newpath}");
                    exit(2);
                }
            }
            Err(_) => {
                eprintln!("File or directory does not exist");
                exit(2);
            }
        }
    }
}

fn update(dotmanurl: &str) {
    println!("Run the folowing command to update dotman:");
    println!();
    println!("sudo wget -O /usr/bin/dotman \"{dotmanurl}\" && sudo chmod +x /usr/bin/dotman",);
}

fn help() {
    let text = "Usage:
  dotman [action] [files...]

MISCELLANEOUS
  -u, update            tells you how to update to the latest version of dotman
  -h, --help, help      displays this help menu

PULL
  -p, pull              copy files from dotfiles directory to home directory
  -rp, rootpull         copy files from dotfiles directory to root user's home directory
  -sp, slashpull        copy files from dotfiles directory to the root directory

PUSH
  -pu, push             copy files from home directory to dotfiles directory
  -spu, slashpush       copy files from the root directory to the dotfiles directory

ADD
  -a, add               add a file to be tracked in the home directory
  -sa, slashadd         add a file to be tracked in the root directory

REMOVE
  -r, remove            remove a file from being tracked in the home directory
  -sr, slashremove      remove a file from being tracked in the root directory

ADDITIONAL INFORMATION
  - the root directory is called \"slash\" in the commands for clarification
  - the ADD commands create a blank file in the dotfiles directory without copying the file

CONFIGURATION INFORMATION
  - the configuration file is dotman.ini in your configuration directory";

    println!("{text}");
}
