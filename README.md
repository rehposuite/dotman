# Dotman

## Name
dotman (DOTfile MANager)

## Description
A simple program that manages your dotfiles (even outside the home directory). The dependencies are `rsync` and `sudo`. Optional dependencies are `wget` and `chmod` if you want to update dotman from the program.

## Installation
Run the `install` script to install dotman

## Usage
#### Taken from `dotman -h`:

    Usage:
      dotman [action] [files...]
    
    MISCELLANEOUS
      -u, update            tells you how to update to the latest version of dotman
      -h, --help, help      displays this help menu
    
    PULL
      -p, pull              copy files from dotfiles directory to home directory
      -rp, rootpull         copy files from dotfiles directory to root user's home directory
      -sp, slashpull        copy files from dotfiles directory to the root directory
    
    PUSH
      -pu, push             copy files from home directory to dotfiles directory
      -spu, slashpush       copy files from the root directory to the dotfiles directory
    
    ADD
      -a, add               add a file to be tracked in the home directory
      -sa, slashadd         add a file to be tracked in the root directory
    
    REMOVE
      -r, remove            remove a file from being tracked in the home directory
      -sr, slashremove      remove a file from being tracked in the root directory
    
    ADDITIONAL INFORMATION
      - the root directory is called "slash" in the commands for clarification
      - the ADD commands create a blank file in the dotfiles directory without copying the file
    
    CONFIGURATION INFORMATION
      - the configuration file is dotman.ini in your configuration directory 

## Support
To get help, just post an issue on GitLab and I will try to help.

## Roadmap
Check if `rsync` is installed  
Config file to read dotfiles directory location

## License
GNU GPLv3
